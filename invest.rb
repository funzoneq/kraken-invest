require 'yaml'
require 'pp'
require 'kraken_client'
require_relative 'lib/kraken_invest'

config = YAML.load_file(File.expand_path('~/.kraken.yml'))

KrakenClient.configure do |c|
      c.api_key     = config['kraken']['api_key']
      c.api_secret  = config['kraken']['api_secret']
      c.base_uri    = 'https://api.kraken.com'
      c.api_version = 0
      c.limiter     = true
      c.tier        = 2
end

client = KrakenClient.load
ki = KrakenInvest.new(client, config['amount_to_buy'], config['buy_pairs'], config['decimals'])

config['buy_pairs'].each do |k,pair|
  ki.buy(pair[:code])
end
