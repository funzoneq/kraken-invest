require 'logger'

class KrakenInvest
	def initialize(client, amount_to_buy, buy_pairs, decimals = 8)
		@client = client
		@amount_to_buy = amount_to_buy
		@decimals = decimals
		@buy_pairs = buy_pairs
		$logger = Logger.new('KrakenInvest.log', 'monthly')
	end

	def getAskPrice(paircode)
		ask_price = "%0.8f" % @client.public.ticker(paircode)[paircode]['a'].first
		@buy_pairs[paircode][:ask_price] = ask_price.to_f
		ask_price.to_f
	end

	def askPriceOkay?(paircode)
		ask_price = self.getAskPrice(paircode)

		if ask_price > @buy_pairs[paircode][:min] and ask_price < @buy_pairs[paircode][:max]
			return true
		end

		$logger.error "Somethings wrong. Found ask price of #{ask_price} for #{paircode}"
		return false
	end

	def getAmountToBuy(paircode)
		ask_price = self.getAskPrice(paircode)

		to_buy = (@amount_to_buy / ask_price).round(@decimals)

		to_buy
	end

	def buy(paircode)
		ask_price = getAskPrice(paircode)

		if askPriceOkay?(paircode)
			to_buy = self.getAmountToBuy(paircode)

			$logger.info "Buying #{@amount_to_buy} euro #{paircode} at #{ask_price} is #{to_buy}"

			opts = {
			  pair: paircode,
			  type: 'buy',
			  ordertype: 'market',
			  volume: to_buy
			}

			@client.private.add_order(opts)
		end
	end
end
